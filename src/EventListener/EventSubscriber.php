<?php

namespace App\EventListener;

use App\Entity\Commande;
use App\Entity\User;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class EventSubscriber implements \Doctrine\Common\EventSubscriber
{

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
        $this->calculateOrderAmount($args);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->indexPrePersist($args);
        $this->calculateOrderAmount($args);
    }

    public function indexPrePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof \App\Entity\Commande) {

            $entity->setDatetime(new \DateTime());
            $entity->setStatus("Prise");
        }
    }

    public function calculateOrderAmount(LifecycleEventArgs $args){

        $entity = $args->getObject();

        if ($entity instanceof Commande){

            $dishes = $entity->getPlats();

            $amountOrder = 0.0;
            foreach ($dishes as $dish){
                $amountOrder += $dish->getPrice();
            }

            $entity->setPrix($amountOrder);
        }
    }
}
