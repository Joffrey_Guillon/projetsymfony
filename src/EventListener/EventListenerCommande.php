<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 02/02/2019
 * Time: 11:05
 */

namespace App\EventListener;


use App\Entity\Commande;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Event\OrderPayedEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Templating\EngineInterface;

class EventListenerCommande
{
    private $mailer;
    private $templating;
    private $dispatcher;

    public function __construct(\Swift_Mailer $mailer,EngineInterface $templating,EventDispatcher $dispatcher)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->dispatcher = $dispatcher;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Commande) {
            return;
        }

        if ($entity->getStatus() === "préparée"){
            $message = (new \Swift_Message('You Got Mail!'))
                ->setFrom('joffrey.guillon23@gmail.com')
                ->setTo('joffrey.guillon@etu.unilim.fr')->setBody(
                $this->templating->render('emails/commandePreparee.html.twig',[
                    'plats' => $entity->getPlats(),
                    'table' => $entity->getTableOrder()
                ]),
                'text/html'
            );
            $this->mailer->send($message);
        }

        if ($entity->getStatus() === "servie"){
            $message = (new \Swift_Message('You Got Mail!'))
                ->setFrom('joffrey.guillon23@gmail.com')
                ->setTo('joffrey.guillon@etu.unilim.fr')->setBody(
                    $this->templating->render('emails/commandeServie.html.twig',[
                        'plats' => $entity->getPlats(),
                        'table' => $entity->getTableOrder()
                    ]),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        if ($entity->getStatus() === "payée"){
            $message = (new \Swift_Message('You Got Mail!'))
            ->setFrom('joffrey.guillon23@gmail.com')
            ->setTo('joffrey.guillon@etu.unilim.fr')->setBody(
                $this->templating->render('emails/commandePayee.html.twig',[
                    'plats' => $entity->getPlats(),
                    'table' => $entity->getTableOrder()
                ]),
                'text/html'
            );
            $this->mailer->send($message);

            $event = new OrderPayedEvent($entity);

            $this->dispatcher->dispatch(OrderPayedEvent::NAME, $event);

        }
    }
}
