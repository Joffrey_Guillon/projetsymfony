<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 04/02/2019
 * Time: 21:42
 */

namespace App\EventListener;


use App\Entity\Commande;
use App\Services\RhService;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class OrderPayedListener
{
    private $serviceRH;

    public function __construct(RhService $rhService)
    {
        $this->serviceRH = $rhService;
    }

    public function postUpdate(LifecycleEventArgs $args){
        $entity = $args->getObject();

        if (!$entity instanceof Commande) {
            return;
        }

        if ($entity->getStatus() === "payée"){
            $this->serviceRH->setOrderPayed($entity);
        }

    }

}
