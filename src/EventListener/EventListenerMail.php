<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 30/01/2019
 * Time: 17:08
 */

namespace App\EventListener;

use App\Entity\Commande;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class EventListenerMail
{
    private $mailer;
    private $templating;

    public function __construct(\Swift_Mailer $mailer,EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Commande) {
            return;
        }

        $message = (new \Swift_Message('You Got Mail!'))
            ->setFrom('joffrey.guillon23@gmail.com')
            ->setTo('joffrey.guillon@etu.unilim.fr')
            ->setBody(
                $this->templating->render('emails/newCommande.html.twig',[
                    'plats' => $entity->getPlats(),
                    'table' => $entity->getTableOrder()
                ]),
                'text/html'
            );

        $this->mailer->send($message);
    }
}
