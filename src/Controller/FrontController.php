<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Dish;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class FrontController extends Controller
{

    /**
     * @Route("/", name="front_home")
     */
    public function index()
    {
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController'
        ]);
    }

    /**
     * @Route("/equipe", name="front_team", methods="GET")
     */
    public function team()
    {

        //$users = $this->getDoctrine()->getRepository(User::class)->findAll();

        $rhService = $this->get('restau.services.rh');

        $team = $rhService->getDayTeam(\date('Y-m-d'));

        return $this->render('front/team/team.html.twig', [
            'controller_name' => 'FrontController',
            "users" => $team
        ]);
    }

    /**
     * @Route("/carte/", name="front_dishes", methods="GET")
     */
    public function carte()
    {
        $nbPlats = array();
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        foreach ($categories as $categorie) {
            $nbPlats[$categorie->getId()] = $this->getDoctrine()->getRepository(Dish::class)->getNbPlat($categorie->getId());
        }

        return $this->render('front/carte/carte.html.twig', [
            'categories' => $categories,
            'nbPlats' => $nbPlats
        ]);
    }

    /**
     * @Route("/carte/{id}", name="front_details_category", methods="GET")
     */
    public function showCategory($id)
    {

        $details = $this->getDoctrine()->getRepository(Category::class)->find($id);

        $plats = $this->getDoctrine()->getRepository(Dish::class)->getPlats($id);

        if (!empty($details)){
            return $this->render('front/carte/category.html.twig', [
                'details' => $details,
                'plats' => $plats
            ]);
        }else{
            throw $this->createNotFoundException("La catégorie n'existe pas !");
        }
    }


    /**
     * @Route("/mentions-legales", name="front_legals", methods={"GET"})
     */
    public function mentions()
    {
        return $this->render('front/mentions.html.twig', [

        ]);
    }

}