<?php

namespace App\Controller;

use App\Entity\Allergen;
use App\Entity\Category;
use App\Entity\Dish;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/equipe/inserer/{username}/{firstname}/{lastname}/{email}/{jobtitle}", name="admin_team_insert", methods="GET")
     */
    public function insertTeam($username,$email,$firstname,$lastname,$jobtitle)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $user->setUsername($username);
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setEmail($email);
        $user->setJobtitle($jobtitle);
        $user->setEnabled(true);

        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('front_team');
    }

    /**
     * @Route("/admin/dishes/inserer")
     */
    public function insertDishesFromJSON(){
        $path = '../dishes.json';

        $em = $this->getDoctrine()->getManager();
        $json = file_get_contents($path);

        $datas = json_decode($json);

        $desserts = $datas->{"desserts"};

        foreach ($desserts as $dessert){
            $dish = new Dish();
            $category = $this->getDoctrine()->getRepository(Category::class)->find(5);
            $dish->setCategory($category);
            $dish->setName($dessert->{"name"});
            $dish->setCalories($dessert->{"calories"});
            $dish->setPrice(floatval($dessert->{"price"}));
            $dish->setUmage($dessert->{"image"});
            $dish->setDescription($dessert->{"text"});
            $dish->setSticky($dessert->{"sticky"});

            foreach ($dessert->allergens as $allergen){
                $allergenExists = $this->getDoctrine()->getRepository(Allergen::class)->findOneBy(array('name' => $allergen));
                if (!$allergenExists){
                    $allergenExists = new Allergen();
                    $allergenExists->setName($allergen);
                }
                $dish->addAllergen($allergenExists);
                $em->persist($dish);
            }
            $em->persist($dish);
        }

        $entrees = $datas->{"entrees"};

        foreach ($entrees as $entree){
            $dish = new Dish();
            $category = $this->getDoctrine()->getRepository(Category::class)->find(3);
            $dish->setCategory($category);
            $dish->setName($entree->{"name"});
            $dish->setCalories($entree->{"calories"});
            $dish->setPrice(floatval($entree->{"price"}));
            $dish->setUmage($entree->{"image"});
            $dish->setDescription($entree->{"text"});
            $dish->setSticky($entree->{"sticky"});

            foreach ($entree->allergens as $allergen){
                $allergenExists = $this->getDoctrine()->getRepository(Allergen::class)->findOneBy(array('name' => $allergen));
                if (!$allergenExists){
                    $allergenExists = new Allergen();
                    $allergenExists->setName($allergen);
                }
                $dish->addAllergen($allergenExists);
                $em->persist($dish);
            }
            $em->persist($dish);
        }

        $plats = $datas->{"plats"};
        foreach ($plats as $plat){
            $dish = new Dish();
            $category = $this->getDoctrine()->getRepository(Category::class)->find(4);
            $dish->setCategory($category);
            $dish->setName($plat->{"name"});
            $dish->setCalories($plat->{"calories"});
            $dish->setPrice(floatval($plat->{"price"}));
            $dish->setUmage($plat->{"image"});
            $dish->setDescription($plat->{"text"});
            $dish->setSticky($plat->{"sticky"});

            foreach ($plat->allergens as $allergen){
                $allergenExists = $this->getDoctrine()->getRepository(Allergen::class)->findOneBy(array('name' => $allergen));
                if (!$allergenExists){
                    $allergenExists = new Allergen();
                    $allergenExists->setName($allergen);
                }
                $dish->addAllergen($allergenExists);
                $em->persist($dish);
            }
            $em->persist($dish);
        }
        $em->flush();

        return $this->redirectToRoute('front_dishes');
    }
}
