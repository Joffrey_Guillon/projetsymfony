<?php

namespace App\Repository;

use App\Entity\Dish;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Dish|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dish|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dish[]    findAll()
 * @method Dish[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DishRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Dish::class);
    }

    public function getNbPlat($id){

        $qb =$this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->andWhere('d.category =:id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult();

        return intval($qb[0][1]);

    }

    public function getPlats($category){
        $qb =$this->createQueryBuilder('d')
            ->select('d.name,d.calories,d.price,d.description,d.umage')
            ->andWhere('d.category =:id')
            ->setParameter('id',$category)
            ->getQuery()
            ->getResult();

        return $qb;
    }

    public function find3lastDishes($max){
        $result = $this->createQueryBuilder('d')
            ->andWhere('d.category = 4')
            ->andWhere('d.sticky =true')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();

        return $result;
    }

    // /**
    //  * @return Dish[] Returns an array of Dish objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dish
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
