<?php

namespace App\Command;

use App\Controller\AdminController;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportPeopleCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'restau:people:import';
    private $adminController;

    public function __construct(AdminController $adminController)
    {
        $this->adminController = $adminController;

        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Import people for restaurant')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);


        $em = $this->adminController->getDoctrine()->getManager();

        $rhService = $this->adminController->get('restau.services.rh');

        $people = $rhService->getPeople();

        foreach ($people as $member){
            $userToAdd = $rhService->convertInUser($member);
            $em->persist($userToAdd);
        }

        $em->flush();

        $io->success('You have import new peoples !');
    }
}
