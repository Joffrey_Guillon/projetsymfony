<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AllergenRepository")
 */
class Allergen
{

    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var ArrayCollection dish $dishes
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Dish",cascade={"persist"},mappedBy="allergens")
     */
    private $dishes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDishes(){

        $tabDish = array();
        foreach ($this->dishes as $dish){
            array_push($tabDish,$dish);
        }
        return $tabDish;
    }

    public function addDish(Dish $dish): ?dish
    {
        if(!$this->dishes->contains($dish)){
            $this->dishes->add($dish);
        }
    }

    public function removeDish(dish $dish): self
    {
        $this->dishes->remove($dish);

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
