<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandeRepository")
 */
class Commande
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commandes",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $serveur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TableResto", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $tableOrder;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Dish", inversedBy="commandes")
     */
    private $plats;

    /**
     * @ORM\Column(type="float")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    public function __construct()
    {
        $this->plats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getServeur(): ?User
    {
        return $this->serveur;
    }

    public function setServeur(?User $serveur): self
    {
        $this->serveur = $serveur;

        return $this;
    }

    public function getTableOrder(): ?TableResto
    {
        return $this->tableOrder;
    }

    public function setTableOrder(?TableResto $tableOrder): self
    {
        $this->tableOrder = $tableOrder;

        return $this;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getPlats(): Collection
    {
        return $this->plats;
    }

    public function addPlat(Dish $plat): self
    {
        if (!$this->plats->contains($plat)) {
            $this->plats[] = $plat;
        }

        return $this;
    }

    public function removePlat(Dish $plat): self
    {
        if ($this->plats->contains($plat)) {
            $this->plats->removeElement($plat);
        }

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
