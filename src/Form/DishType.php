<?php

namespace App\Form;

use App\Entity\Allergen;
use App\Entity\Dish;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,[
                'required' => true
            ])
            ->add('calories',ChoiceType::class,[
                'choices' => $this->_availableCalories()
            ])
            ->add('price',MoneyType::class,[
                'required' => true
            ])
            ->add('umage',null,[
                'required' => false,
                'empty_data' => 'http://via.placeholder.com/360x225'
            ])
            ->add('description',null,[
                'required' => true
            ])
            ->add('sticky')
            ->add('category',null,[
                'required' => true
            ])
            ->add('allergens')
            ->add('user');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dish::class,
        ]);
    }

    public function _availableCalories()
    {
        $calories = array ();
        for ( $i = 10 ; $i <= 300 ; $i += 10 )
            $calories [ $i ]= $i ;
        return $calories ;
    }
}
